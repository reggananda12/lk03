class Pertambahan extends Kalkulator {
    //Do your magic here...
    double operan1,operan2;

    public void setOperan(double operand1, double operand2) {
        this.operan1 = operand1;
        this.operan2 = operand2;
     }

     public double hitung() {
        return operan1 + operan2;
    }
}